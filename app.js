const fs = require('fs');

var input = require("./output.json");

var places = input.timelineObjects;
var output = [];
for (var i = 0; i < places.length; i++) {
	var place = places[i].placeVisit;
	var newPlace = {};
	newPlace["type"] = "Feature";
	newPlace["geometry"] = {
		"type": "Point",
		"coordinates": [place.location.longitudeE7/1e7, place.location.latitudeE7/1e7]
	}
	newPlace["properties"] = {
		name: place.location.name,
		duration: place.duration
	}
	output.push(newPlace);
}

var collection ={
	"type": "FeatureCollection",
	"features": output
}


var jsonContent = JSON.stringify(collection);
fs.writeFile("geojson.json", jsonContent, 'utf8', function (err) {
    if (err) {
        console.log("An error occured while writing JSON Object to File.");
        return console.log(err);
    }
 
    console.log("JSON file has been saved.");
});